from django import forms
from .models import *

class OrmawaForm(forms.ModelForm):
    class Meta:
        model = Ormawa
        fields = ['nama', 'singkatan', 'deskripsi', 'level', 'status']
        widgets = {
            'nama': forms.TextInput(attrs={
                'x-model': "ormawaEdit.nama",
                'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                }),
            'singkatan': forms.TextInput(attrs={
                'x-model': "ormawaEdit.singkatan",
                'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                }),
            'deskripsi': forms.Textarea(attrs={
                'x-model': "ormawaEdit.deskripsi",
                'rows': '2', 
                'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                }),
            'level': forms.Select(attrs={
                'x-model': "ormawaEdit.jenis",
                'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                }),
            'status': forms.NumberInput(attrs={
                'x-model': "ormawaEdit.status",
                'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                }),
        }

class LevelForm(forms.ModelForm):
    class Meta:
        model = Level
        fields = ['nama']
        widgets = {
            'nama': forms.TextInput(attrs={
                'x-model': "levelOrmawaEdit.nama",
                'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                }),
        }

class KepengurusanForm(forms.Form):
    kepengurusan = forms.CharField(
        label="Nama Kepengurusan",
        widget=forms.TextInput(
            attrs={
                'placeholder':'Masukan Nama Organisasi',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            },
        ),
    )
    ketua = forms.CharField(
        label="Ketua",
        widget=forms.TextInput(
            attrs={
                'x-model': 'searchQuery.ketua',
                '@click': 'show.ketua = true',
                '@click.outside': 'reset("ketua")',
                'placeholder':'Masukan NIM Mahasiswa',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            },
        ),
    )
    # selectedKetua = forms.CharField(
    #     widget=forms.TextInput(
    #         attrs={
    #             'x-model': 'selected.ketua',
    #             '@keyup.delete': 'resetSelected("ketua");',
    #             '@keyup.backspace': 'resetSelected("ketua");',
    #             'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
    #         },
    #     ),
    # )
    sekretaris = forms.CharField(
        label="Sekretaris",
        widget=forms.TextInput(
            attrs={
                'x-model': 'searchQuery.sekretaris',
                '@click': 'show.sekretaris = true',
                '@click.outside': 'reset("sekretaris")',
                'placeholder':'Masukan NIM Mahasiswa',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            },
        ),
    )
    # selectedSekretaris = forms.CharField(
    #     widget=forms.TextInput(
    #         attrs={
    #             'x-model': 'selected.sekretaris',
    #             '@keyup.delete': 'resetSelected("sekretaris");',
    #             '@keyup.backspace': 'resetSelected("sekretaris");',
    #             'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
    #         },
    #     ),
    # )
    # periode = forms.ChoiceField(
    #     label="Periode",
    #     widget=forms.Select(
    #         attrs={
    #             'x-model': 'searchQuery',
    #             'placeholder':'Pilih Periode',
    #             'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
    #         },
    #     ),
    # )
    # def __init__(self, *args, **kwargs):
    #     super(KepengurusanForm, self).__init__(*args, **kwargs)
    #     empty_choice = [('', 'Pilih Periode')]
    #     self.fields['periode'].choices = empty_choice + list(Periode.objects.all().values_list('nama', 'nama'))
   
class PeriodeForm(forms.ModelForm):
    class Meta:     
        model = Periode
        fields = ['nama', 'tgl_mulai', 'tgl_selesai']
        labels = {
            'nama': 'Nama Periode'
        }
        widgets = {
            'nama': forms.TextInput(attrs={
                'x-model': "periodeEdit.nama",
                'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                }),
            'tgl_mulai': forms.DateInput(
                format=('%d-%m-%Y'), 
                attrs={
                    'x-model': "periodeEdit.tgl_mulai",
                    'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                    'type': 'date',
                    },
            ),
            'tgl_selesai': forms.DateInput(
                format=('%d-%m-%Y'), 
                attrs={
                    'x-model': "periodeEdit.tgl_selesai",
                    'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                    'type': 'date',
                    },
            ),
        }
        
class KepengurusanSearchForm(forms.Form):
    organisasi = forms.CharField(
        label="Nama Organisasi",
        widget=forms.TextInput(
            attrs={
                'x-model': 'searchQuery',
                '@click': 'ormawaShow = true',
                '@click.outside': 'reset()',
                'placeholder':'Pilih Nama Organisasi',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            },
        ),
        required=False,
    )

    selectedOrganisasi = forms.CharField(
        label="Nama Organisasi",
        widget=forms.TextInput(
            attrs={
                'x-model': 'selectedOrmawa',
                '@keyup.delete': 'resetSelected();',
                '@keyup.backspace': 'resetSelected();',
                'placeholder':'Pilih',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            },
        ),
    )

    selectedOrmawaSingkatan = forms.CharField(
        label="Selected Ormawa Singkatan Hidden",
        widget=forms.TextInput(
            attrs={
                'x-model': 'selectedOrmawaSingkatan',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            },
        ),
    )

    periode = forms.ChoiceField(
        label="Periode", 
        widget=forms.Select(
            attrs={
                'x-model': 'selectedPeriode',
                'class': 'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            },
        ),
    )
    def __init__(self, *args, **kwargs):
        super(KepengurusanSearchForm, self).__init__(*args, **kwargs)
        empty_choice = [('', 'Pilih Periode')]
        self.fields['periode'].choices = empty_choice + list(Periode.objects.all().values_list('nama', 'nama'))
        
class PengurusForm(forms.ModelForm):
   class Meta:     
        model = Pengurus
        fields = ['mahasiswa', 'struktur']
        labels = {
            'mahasiswa': 'Mahasiswa',
            'struktur': 'Jabatan',
        }
        widgets = {
            'mahasiswa': forms.Select(
                attrs={
                    'x-model': "pengurusEdit.idMahasiswa",
                    'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                },
            ),
            'struktur': forms.Select(
                attrs={
                    'x-model': "pengurusEdit.idStruktur",
                    'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                },
            ),
        }

class StrukturOrgForm(forms.ModelForm):
    class Meta:
        model = StrukturOrg
        fields = ['nama']
        labels = {
            'nama': 'Nama Jabatan',
        }
        widgets = {
            'nama': forms.TextInput(
                attrs={
                  'x-model':'namaJabatan',
                  'placeholder':'Nama Jabatan',
                  'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                },
            ),
        }

class KegiatanForm(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = [
            'nama', 
            'tgl_mulai', 
            'tgl_selesai', 
            'deskripsi', 
        ]
    nama = forms.CharField(
        label="Nama",
        widget=forms.TextInput(
            attrs={
                'x-model':'kegiatanEdit.nama',
                'placeholder':'Nama Jabatan',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )    
    tgl_mulai = forms.DateField(
        label="Tanggal Mulai",
        widget=forms.DateInput(
            attrs={
                'x-model':'kegiatanEdit.tgl_selesai',
                'placeholder':'Nama Jabatan',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                'type': 'date',
            }
        )
    )
    tgl_selesai = forms.DateField(
        label="Tanggal Selesai",
        widget=forms.DateInput(
            attrs={
                'x-model':'kegiatanEdit.tgl_selesai',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                'type': 'date',
            }
        )
    )
    deskripsi = forms.CharField(
        label="Deskripsi",
        widget=forms.Textarea(
            attrs={
                'x-model':'kegiatanEdit.deskripsi',
                'rows': '3',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )