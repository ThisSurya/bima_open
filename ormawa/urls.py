from django.urls import path
from django.contrib.auth.decorators import login_required
from . import views

# Ormawa
urlpatterns = [
    path("ormawa", login_required(views.OrmawaView.index), name="ormawa.index"),
    path("ormawa/search", login_required(views.OrmawaView.search), name="ormawa.search"),
    path("ormawa/<int:ormawaID>", login_required(views.OrmawaView.show), name="ormawa.show"),
    path("ormawa/create", login_required(views.OrmawaView.create), name="ormawa.create"),
    path("ormawa/edit/<int:ormawaID>", login_required(views.OrmawaView.edit), name="ormawa.edit"),
    path("ormawa/search", login_required(views.OrmawaView.search), name="ormawa.search"),
    path("ormawa/delete/<int:ormawaID>", login_required(views.OrmawaView.delete), name="ormawa.delete"),
]

# Jenis Ormawa / Level
urlpatterns += [
    path("jenis-ormawa", login_required(views.LevelView.index), name="level.index"),
    path("jenis-ormawa/create", login_required(views.LevelView.create), name="level.create"),
    path("jenis-ormawa/edit/<int:levelID>", login_required(views.LevelView.edit), name="level.edit"),
    path("jenis-ormawa/delete/<int:levelID>", login_required(views.LevelView.delete), name="level.delete"),
]

# Kepengurusan
urlpatterns += [
    path("kepengurusan", login_required(views.KepengurusanView.index), name="kepengurusan.index"),
    path("kepengurusan/show/<str:ormawaName>/<str:periodeName>", login_required(views.KepengurusanView.show), name="kepengurusan.show"),
    path("kepengurusan/create/<str:ormawaName>/<str:periodeName>", login_required(views.KepengurusanView.create), name="kepengurusan.create"),
    path("kepengurusan/<int:kepengurusanID>/edit", login_required(views.KepengurusanView.edit), name="kepengurusan.edit"),
    path("kepengurusan/<int:kepengurusanID>/delete", login_required(views.KepengurusanView.delete), name="kepengurusan.delete"),
]
    
# Periode
urlpatterns += [
    path("periode", login_required(views.PeriodeView.index), name="periode.index"),
    path("periode/create", login_required(views.PeriodeView.create), name="periode.create"),
    path("periode/<int:periodeID>/edit", login_required(views.PeriodeView.edit), name="periode.edit"),
    path("periode/<int:periodeID>/delete", login_required(views.PeriodeView.delete), name="periode.delete"),
]

# Pengurus
urlpatterns += [
    path("pengurus", login_required(views.PengurusView.index), name="pengurus.index"),
    path("pengurus/create/<int:kepengurusanID>", login_required(views.PengurusView.create), name="pengurus.create"),
    path("pengurus/<int:pengurusID>/edit", login_required(views.PengurusView.edit), name="pengurus.edit"),
    path("pengurus/<int:pengurusID>/delete", login_required(views.PengurusView.delete), name="pengurus.delete"),
]

# Struktur Organisasi
urlpatterns += [
    path("struktur-organisasi", login_required(views.StrukturOrgView.index), name="strukturorg.index"),
    path("struktur-organisasi/create/<int:ormawaID>", login_required(views.StrukturOrgView.create), name="strukturorg.create"),
    path("struktur-organisasi/<int:strukturOrgID>/edit", login_required(views.StrukturOrgView.edit), name="strukturorg.edit"),
    path("struktur-organisasi/<int:strukturOrgID>/delete", login_required(views.StrukturOrgView.delete), name="strukturorg.delete"),
]

# Kegiatan
urlpatterns += [
    path("kegiatan", login_required(views.KegiatanView.index), name="kegiatan.index"),
    path("kegiatan/create", login_required(views.KegiatanView.create), name="kegiatan.create"),
    path("kegiatan/edit/<int:kegiatanID>", login_required(views.KegiatanView.edit), name="kegiatan.edit"),
    path("kegiatan/delete/<int:kegiatanID>", login_required(views.KegiatanView.delete), name="kegiatan.delete"),
]
