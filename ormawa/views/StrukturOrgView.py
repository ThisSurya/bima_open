from django.shortcuts import render, redirect
from django.http import JsonResponse, HttpResponse
from ormawa.forms import StrukturOrgForm
from ormawa.models import StrukturOrg, Kepengurusan
from django.db.models import Q
from django.contrib.auth.decorators import permission_required


class StrukturOrgView:

    def index(request):
        form = StrukturOrgForm()

        kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(ketua__nim=request.user)).first()
        strukturOrg = StrukturOrg.objects.filter(ormawa=kepengurusan.ormawa)

        context = {
            "form": form,
            "strukturOrg": strukturOrg
        }
        return render(request, "user/strukturOrg/index.html", context)


    def create(request, ormawaID):
        if request.method == "POST":
            form = StrukturOrgForm(request.POST)
            if form.is_valid():
                if request.user.groups.filter(name="BIMA UNIVERSITAS").exists():
                    strukturOrg = form.save(commit=False)
                    strukturOrg.active = True
                    strukturOrg.ormawa_id = ormawaID
                    strukturOrg.save()
                    
                    return JsonResponse({
                        'id': strukturOrg.id,
                        'nama': strukturOrg.nama,
                        'active': 'True' if strukturOrg.active else 'False',
                    })
                elif request.user.groups.filter(name="BIMA ORGANISASI").exists():
                    kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
                    strukturOrg = form.save(commit=False)
                    strukturOrg.ormawa = kepengurusan.ormawa
                    strukturOrg.active = True
                    strukturOrg.save()

                    return redirect('sktrukturorg.index')

            else:
                return HttpResponse("<p>Error wak</p>")
        return
    
    def edit(request, strukturOrgID):
        if request.method == "POST":
            if request.user.groups.filter(name="BIMA UNIVERSITAS").exists():
                # edit status
                strukturOrg = StrukturOrg.objects.filter(id=strukturOrgID).first()
                strukturOrg.active = int(request.POST["status"])
                strukturOrg.save()
                
                return JsonResponse({
                        'id': strukturOrg.id,
                        'nama': strukturOrg.nama,
                        'active': 'True' if strukturOrg.active else 'False',
                    })
            
            elif request.user.groups.filter(name="BIMA ORGANISASI").exists():
                # edit status and name
                kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(ketua__nim=request.user)).first()
                strukturOrg = StrukturOrg.objects.filter(id=strukturOrgID, ormawa=kepengurusan.ormawa).first()
                form = StrukturOrgForm(request.POST, instance=strukturOrg)
                if form.is_valid():
                    strukturOrg.active = 1
                    strukturOrg.save()

                    return redirect('strukturorg.index')

        return
    
    def delete(request, strukturOrgID):
        if request.method == "POST":
            if request.user.groups.filter(name="BIMA UNIVERSITAS").exists():
                strukturOrg = StrukturOrg.objects.filter(id=strukturOrgID).first()
                strukturOrg.delete()
                
                return JsonResponse({
                        'status': 'success',
                    })
            
            elif request.user.groups.filter(name="BIMA ORGANISASI").exists():
                kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(ketua__nim=request.user)).first()
                strukturOrg = StrukturOrg.objects.filter(id=strukturOrgID, ormawa=kepengurusan.ormawa).first()
                strukturOrg.delete()
                
                return redirect('strukturorg.index')
