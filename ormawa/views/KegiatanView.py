from django.shortcuts import render, redirect
from ormawa.forms import KegiatanForm
from ormawa.models import Kegiatan, Kepengurusan
from django.db.models import Q
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.models import User

class KegiatanView:

    @permission_required('ormawa.view_kegiatan')
    def index(request):
        form = KegiatanForm()

        kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
        kegiatan = Kegiatan.objects.filter(kepengurusan=kepengurusan)

        context = {
            "form": form,
            "kegiatan": kegiatan,
        }
        return render(request, 'user/kegiatan/index.html', context)
    
    @permission_required('ormawa.add_kegiatan')
    def create(request):
        if request.method == "POST":
            form = KegiatanForm(request.POST)
            if form.is_valid():
                kegiatan = form.save(commit=False)
                kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
                kegiatan.kepengurusan = kepengurusan
                kegiatan.ormawa = kepengurusan.ormawa
                kegiatan.periode = kepengurusan.periode
                kegiatan.save()
                return redirect('kegiatan.index')
    
    @permission_required('ormawa.change_kegiatan')
    def edit(request, kegiatanID):
        if request.method == "POST":
            kegiatan = Kegiatan.objects.get(id=kegiatanID)
            form = KegiatanForm(request.POST, instance=kegiatan)
            if form.is_valid():
                form.save()
                return redirect('kegiatan.index')

    @permission_required('ormawa.delete_kegiatan')
    def delete(request, kegiatanID):
        if request.method ==  "POST":
            Kegiatan.objects.get(id=kegiatanID).delete()

            return redirect('kegiatan.index')
