from django.shortcuts import redirect, render
from ormawa.forms import PengurusForm
from ormawa.models import Pengurus, Kepengurusan
from django.db.models import Q
from django.contrib.auth.decorators import permission_required


class PengurusView:
    def index(request):
        form = PengurusForm()
        kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
        pengurus = Pengurus.objects.filter(kepengurusan=kepengurusan)

        context = {
            "form": form,
            "pengurus": pengurus,
        }
        return render(request, 'user/pengurus/index.html', context)

    @permission_required('ormawa.add_pengurus')
    def create(request, kepengurusanID):
        if request.method == "POST":
            form = PengurusForm(request.POST)
            if form.is_valid():
                pengurus = form.save(commit=False)
                
                if kepengurusanID != 0 and request.user.groups.filter(name="BIMA UNIVERSITAS").exists():
                    pengurus.kepengurusan_id = kepengurusanID
                    pengurus.save()

                    return redirect('kepengurusan.show', pengurus.kepengurusan.ormawa.singkatan, pengurus.kepengurusan.periode.nama)

                elif kepengurusanID == 0 and request.user.groups.filter(name="BIMA ORGANISASI").exists():
                    kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
                    pengurus.kepengurusan = kepengurusan
                    pengurus.save()

                    return redirect('pengurus.index')

    @permission_required('ormawa.change_pengurus')
    def edit(request, pengurusID):
        if request.method == "POST":
            if request.user.groups.filter(name="BIMA UNIVERSITAS").exists():
                pengurus = Pengurus.objects.filter(id=pengurusID).first()
            elif request.user.groups.filter(name="BIMA ORGANISASI").exists():
                kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
                pengurus = Pengurus.objects.filter(id=pengurusID, kepengurusan=kepengurusan).first()
            
            form = PengurusForm(request.POST, instance=pengurus)
            if form.is_valid():
                pengurus = form.save(commit=False)
                pengurus.save() 

                if request.user.groups.filter(name="BIMA UNIVERSITAS").exists():
                    return redirect('kepengurusan.show', pengurus.kepengurusan.ormawa.singkatan, pengurus.kepengurusan.periode.nama)
                elif request.user.groups.filter(name="BIMA ORGANISASI").exists():
                    return redirect('pengurus.index')

    @permission_required('ormawa.delete_pengurus')
    def delete(request, pengurusID):
        if request.method == "POST":
            if request.user.groups.filter(name="BIMA UNIVERSITAS").exists():
                pengurus = Pengurus.objects.filter(id=pengurusID).first()
                pengurus.delete()
                
                return redirect('kepengurusan.show', pengurus.kepengurusan.ormawa.singkatan, pengurus.kepengurusan.periode.nama)

            elif request.user.groups.filter(name="BIMA ORGANISASI").exists():
                kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
                pengurus = Pengurus.objects.filter(id=pengurusID, kepengurusan=kepengurusan).first()
                pengurus.delete()

                return redirect('pengurus.index')