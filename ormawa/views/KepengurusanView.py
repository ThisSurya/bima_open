from django.shortcuts import render, redirect
from django.http import JsonResponse
from ormawa.forms import *
from ormawa.models import Ormawa, Kepengurusan, Periode, StrukturOrg
from mahasiswa.models import Mahasiswa
from django.contrib.auth.decorators import permission_required

class KepengurusanView:
    
    @permission_required('ormawa.view_kepengurusan')
    def index(request):
        if request.method == "POST":
            form = KepengurusanSearchForm(request.POST)
            if form.is_valid():
                ormawaSingkatan = form.cleaned_data['selectedOrmawaSingkatan']
                periode = Periode.objects.filter(id=form.cleaned_data['periode']).first()
                return redirect('kepengurusan.show', ormawaSingkatan, periode.nama)
            else:
                return redirect('kepengurusan.index')
        else:
            form = KepengurusanSearchForm()
            ormawa = Ormawa.objects.all()
            context = {
                'ormawa': ormawa,
                'form': form,
            }
            return render(request, 'admin/kepengurusan/index.html', context)

    @permission_required('ormawa.view_kepengurusan')
    def show(request, ormawaName, periodeName):
        kepengurusan = Kepengurusan.objects.filter(
            ormawa__singkatan=ormawaName,
            periode__nama=periodeName,
        ).first()
        if kepengurusan:
            strukturOrg = StrukturOrg.objects.filter(ormawa_id=kepengurusan.ormawa_id)
            formPengurus = PengurusForm()
            formStrukturOrg = StrukturOrgForm()

            context = {
                "kepengurusan": kepengurusan,
                "strukturOrg": strukturOrg,

                "formPengurus": formPengurus,
                "formStrukturOrg": formStrukturOrg,
            }
            return render(request, 'admin/kepengurusan/show.html', context)
        else:
            kepengurusanForm = KepengurusanForm()
            context = {
                "kepengurusan": False,
                "kepengurusanForm": kepengurusanForm,
                "ormawa": ormawaName,
                "periode": periodeName,
            }
            return render(request, 'admin/kepengurusan/show.html', context)

    @permission_required('ormawa.add_kepengurusan')
    def create(request, ormawaName, periodeName):
        if request.method == "POST":
            form = KepengurusanForm(request.POST)
            if form.is_valid():
                kepengurusan = Kepengurusan.objects.create(
                    nama=form.cleaned_data['kepengurusan'],
                    ketua=Mahasiswa.objects.get(nim=form.cleaned_data['ketua']),
                    sekretaris=Mahasiswa.objects.get(nim=form.cleaned_data['sekretaris']),
                    periode=Periode.objects.get(nama=periodeName),
                    ormawa=Ormawa.objects.get(singkatan=ormawaName),
                )

                return redirect('kepengurusan.show', kepengurusan.ormawa.singkatan, kepengurusan.periode.nama)

    @permission_required('ormawa.change_kepengurusan')
    def edit(request, kepengurusanID):
        kepengurusan = Kepengurusan.objects.filter(id=kepengurusanID).first()
        periode = Periode.objects.filter(id=kepengurusan.periode.id).first()

        if request.method == "POST":
            periodeForm = PeriodeForm(request.POST, instance=periode)
            kepengurusanForm = KepengurusanForm(request.POST, instance=kepengurusan)
            if periodeForm.is_valid() and kepengurusanForm.is_valid():
                periodeForm.save()
                print(kepengurusanForm.save())
                

                return redirect('ormawa.show', kepengurusan.ormawa_id)
        else:
            data = {
                "id": kepengurusan.id,
                "nama": kepengurusan.nama,
                "ketua": kepengurusan.ketua.id,
                "sekretaris": kepengurusan.sekretaris.id,
                "tgl_mulai": kepengurusan.periode.tgl_mulai,
                "tgl_selesai": kepengurusan.periode.tgl_selesai,
            }

            print(data)
            return JsonResponse(data, safe=False) 
    
    @permission_required('ormawa.delete_kepengurusan')
    def delete(request, kepengurusanID):
        if request.method == "POST":
            kepengurusan = Kepengurusan.objects.filter(id=kepengurusanID).first()
            kepengurusan.delete()
            return redirect('ormawa.show', kepengurusan.ormawa_id)
