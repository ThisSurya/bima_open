from django.shortcuts import render, redirect
from ormawa.forms import *
from ormawa.models import Level
from django.contrib.auth.decorators import permission_required


class LevelView:

    @permission_required('ormawa.view_level')
    def index(request):
        level = Level.objects.all()
        levelForm = LevelForm()
        

        context = {
            "level": level,
            "levelForm": levelForm,
        }
        return render(request, "admin/level/index.html",context)

    @permission_required('ormawa.add_level')
    def create(request):
        if request.method == "POST":
            form = LevelForm(request.POST)
            if form.is_valid():
                level = form.save(commit=False)
                level.save()
                return redirect('level.index')
    
    @permission_required('ormawa.change_level')
    def edit(request, levelID):
        if request.method == "POST":
            level = Level.objects.filter(id=levelID).first()

            form = LevelForm(request.POST, instance=level)
            if form.is_valid():
                level = form.save(commit=False)
                level.save()
                return redirect('level.index')
    
    @permission_required('ormawa.delete_level')
    def delete(request, levelID):
        Level.objects.filter(id=levelID).delete()
        return redirect('level.index')
