from django.shortcuts import render, redirect
from ormawa.forms import *
from ormawa.models import Periode
from django.contrib.auth.decorators import permission_required

class PeriodeView:

    @permission_required('ormawa.view_periode')
    def index(request):
        form = PeriodeForm()
        periode = Periode.objects.all()

        context = {
            'form': form,
            'periode': periode,
        }
        return render(request, 'admin/periode/index.html', context)

    @permission_required('ormawa.add_periode')
    def create(request):
        if request.method == 'POST':
            form = PeriodeForm(request.POST)
            if form.is_valid():
                form.save()

                return redirect('periode.index')

    @permission_required('ormawa.change_periode')
    def edit(request, periodeID):
        if request.method == 'POST':
            periode = Periode.objects.filter(id=periodeID).first()
            form = PeriodeForm(request.POST, instance=periode)
            if form.is_valid():
                form.save()

                return redirect('periode.index')
    
    @permission_required('ormawa.delete_periode')
    def delete(request, periodeID):
        if request.method == 'POST':
            Periode.objects.filter(id=periodeID).delete()           
            return redirect('periode.index')
