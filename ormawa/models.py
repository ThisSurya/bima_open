from django.db import models
from mahasiswa.models import Mahasiswa

class Level(models.Model):
    id = models.AutoField(primary_key=True)
    nama = models.CharField(max_length=100)

    class Meta:
        verbose_name_plural = "Level Ormawa"

    def __str__(self):
        return f'{self.nama}'

class Ormawa(models.Model):
    id = models.AutoField(primary_key=True)
    nama = models.CharField(max_length=100)
    singkatan = models.CharField(max_length=20)
    deskripsi = models.TextField()
    level = models.ForeignKey(Level, on_delete=models.CASCADE)
    status = models.IntegerField()

    class Meta:
        verbose_name_plural = "Ormawa"

    def __str__(self):
        return f'{self.singkatan}'

class Periode(models.Model):
    id = models.AutoField(primary_key=True)
    nama = models.CharField(max_length=100)
    tgl_mulai = models.DateField()
    tgl_selesai = models.DateField()
    
    class Meta:
        verbose_name_plural = "Periode"

    def __str__(self):
        return f'{self.nama}'


class StrukturOrg(models.Model):
    id = models.AutoField(primary_key=True)
    nama = models.CharField(max_length=100)
    ormawa = models.ForeignKey(Ormawa, on_delete=models.CASCADE)
    active = models.BooleanField()
    
    class Meta:
        verbose_name_plural = "Struktur Organisasi"

    def __str__(self):
        return f'{self.nama}'
    
class Kepengurusan(models.Model):
    id = models.AutoField(primary_key=True)
    nama = models.CharField(max_length=100)
    ormawa = models.ForeignKey(Ormawa, on_delete=models.CASCADE)
    periode = models.ForeignKey(Periode, on_delete=models.CASCADE)
    ketua = models.ForeignKey(Mahasiswa, related_name='ketua', on_delete=models.CASCADE)
    sekretaris = models.ForeignKey(Mahasiswa, related_name='sekretaris', on_delete=models.CASCADE)
    
    class Meta:
        verbose_name_plural = "Kepengurusan"
    
    def __str__(self):
        return f'{self.nama} - {self.ormawa} - {self.periode}'

class Pengurus(models.Model):
    id = models.AutoField(primary_key=True)
    mahasiswa = models.ForeignKey(Mahasiswa, on_delete=models.CASCADE)
    struktur = models.ForeignKey(StrukturOrg, on_delete=models.CASCADE)
    kepengurusan = models.ForeignKey(Kepengurusan, on_delete=models.CASCADE)
    
    class Meta:
        verbose_name_plural = "Pengurus"


class Kegiatan(models.Model):
    id = models.AutoField(primary_key=True)
    nama = models.CharField(max_length=100)
    tgl_mulai = models.DateField()
    tgl_selesai = models.DateField()
    deskripsi = models.TextField()
    kepengurusan = models.ForeignKey(Kepengurusan, on_delete=models.CASCADE)
    ormawa = models.ForeignKey(Ormawa, on_delete=models.CASCADE)
    periode = models.ForeignKey(Periode, on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = "Kegiatan"


class Anggaran(models.Model):
    id = models.AutoField(primary_key=True)
    nama = models.CharField(max_length=100)
    qty = models.IntegerField()
    harga = models.IntegerField()
    satuan = models.CharField(max_length=255)
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = "Anggaran"


class Dokumentasi(models.Model):
    id = models.AutoField(primary_key=True)
    judul = models.CharField(max_length=255)
    url = models.CharField(max_length=255)
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = "Dokumentasi"


class Seksi(models.Model):
    id = models.AutoField(primary_key=True)
    nama = models.CharField(max_length=255)
    mahasiswa = models.ForeignKey(Mahasiswa, on_delete=models.CASCADE)
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE)
    koordinator = models.BooleanField()

    class Meta:
        verbose_name_plural = "Seksi"


class Acara(models.Model):
    id = models.AutoField(primary_key=True)
    nama = models.CharField(max_length=255)
    waktu_mulai = models.DateTimeField()
    waktu_selesai = models.DateTimeField()
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE)
    seksi = models.ForeignKey(Seksi, on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = "Acara"