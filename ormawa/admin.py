from django.contrib import admin
from .models import *

# Register your models here.
@admin.register(Level)
class LevelAdmin(admin.ModelAdmin):
    list_display = ['nama']

@admin.register(Ormawa)
class OrmawaAdmin(admin.ModelAdmin):
    list_display = ['nama', 'singkatan', 'deskripsi', 'level', 'status',]

@admin.register(Kepengurusan)
class KepengurusanAdmin(admin.ModelAdmin):
    list_display = ['nama', 'ormawa', 'periode', 'ketua', 'sekretaris',]

@admin.register(Periode)
class PeriodeAdmin(admin.ModelAdmin):
    list_display = ['nama', 'tgl_mulai', 'tgl_selesai',]

@admin.register(Pengurus)
class PengurusAdmin(admin.ModelAdmin):
    list_display = ['mahasiswa', 'struktur', 'kepengurusan',]

@admin.register(StrukturOrg)
class StrukturOrgAdmin(admin.ModelAdmin):
    list_display = ['nama', 'active',]  

@admin.register(Kegiatan)
class KegiatanAdmin(admin.ModelAdmin):
    list_display = [
            'nama', 
            'tgl_mulai', 
            'tgl_selesai', 
            'deskripsi', 
            'ormawa',
            'kepengurusan',
            'periode',
        ]  