$(document).ready(function () {
    $(".dropdown-trigger").on("click", function () {
        $(this).find(".dropdown-menu").toggleClass("hidden");
    });
});

$(document).ready(function () {
    $(".expandKepengurusanOption").on("click", function () {
        $(this).next(".kepengurusanOption").toggleClass("hidden");
    });

    $(document).on("click", function (event) {
        if (!$(event.target).closest(".expandKepengurusanOption").length) {
            $(".kepengurusanOption").addClass("hidden");
        }
    });
});


$(document).ready(function () {
    $(".expandPengurus").on("click", function () {
        var pengurusElement = $(this).closest('.grid').next('.pengurus')
        pengurusElement.toggleClass("hidden");
        if(pengurusElement.hasClass("hidden")){
            $(this).find("i").removeClass("fa-chevron-down")
            $(this).find("i").addClass("fa-chevron-right")
        } else {
            $(this).find("i").removeClass("fa-chevron-right")
            $(this).find("i").addClass("fa-chevron-down")
        }
        var kepengurusanBoxElement = $(this).closest(".grid")
        var bgColor = "bg-royal-blue-100"
        var bgActiveColor = "bg-royal-blue-200"
        var textColor = "text-royal-blue-700"
        var textActiveColor = "text-royal-blue-900"
        if(kepengurusanBoxElement.hasClass(bgActiveColor)){
            kepengurusanBoxElement.removeClass(bgActiveColor)
            kepengurusanBoxElement.addClass(bgColor)
            kepengurusanBoxElement.removeClass(textActiveColor)
            kepengurusanBoxElement.addClass(textColor)
        } else {
            kepengurusanBoxElement.removeClass(bgColor)
            kepengurusanBoxElement.addClass(bgActiveColor)
            kepengurusanBoxElement.removeClass(textColor)
            kepengurusanBoxElement.addClass(textActiveColor)
        }
    });
});