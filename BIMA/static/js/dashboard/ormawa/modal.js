$(document).ready(function () {
  // Ketika tombol dropdown di klik
  $("#modalOpen").click(function () {
    console.log("lololo");
    $("#modal").removeClass("hidden");
  });
  $("#modalClose").click(function () {
    $("#modal").addClass("hidden");
  });
});

$(document).ready(function () {
  $(".editOrmawa").click(function () {
    $("#modal").removeClass("hidden");
    var ormawaID = $(this).data("id");

    $.ajax({
      url: "ormawa/edit/" + ormawaID,
      type: "GET",
      success: function (data) {
        $("#modalForm").attr("action", "ormawa/edit/" + ormawaID);

        $('#modalForm input[name="nama"]').val(data.nama);
        $('#modalForm input[name="singkatan"]').val(data.singkatan);
        $('#modalForm textarea[name="deskripsi"]').val(data.deskripsi);
        $('#modalForm select[name="level"]').val(data.level);
        $('#modalForm input[name="status"]').val(data.status);
      },
    });
  });
});

$(document).ready(function () {
  $("#modalLevelOpen").click(function () {
    $("#modalLevel").removeClass("hidden");
  });
  $("#modalLevelClose").click(function () {
    $("#modalLevel").addClass("hidden");
  });

  $(".editLevel").click(function () {
    var levelID = $(this).data("id");

    $.ajax({
      url: "ormawa/level/edit/" + levelID,
      type: "GET",
      success: function (data) {
        $("#modalLevelForm").attr("action", "ormawa/level/edit/" + levelID);

        $('#modalLevelForm input[name="nama"]').val(data.nama);

        $('#modalLevelForm button[type="submit"]').text("Save");
      },
    });
  });
});

$(document).ready(function () {
  $("#modalKepengurusanOpen").click(function () {
    $("#modalKepengurusan").removeClass("hidden");
  });
  $("#modalKepengurusanClose").click(function () {
    $("#modalKepengurusan").addClass("hidden");
  });
  $(".editKepengurusan").click(function () {
    $("#modalKepengurusan").removeClass("hidden");

    var kepengurusanID = $(this).data("id");

    $.ajax({
      url: "x/kepengurusan/edit/" + kepengurusanID,
      type: "GET",
      success: function (data) {
        $("#modalTitle").text("Edit Kepengurusan");

        $("#modalKepengurusanForm").attr(
          "action",
          "x/kepengurusan/edit/" + kepengurusanID
        );

        $('#modalKepengurusan input[name="nama"]').val(data.nama);
        $('#modalKepengurusan select[name="ketua"]').val(data.ketua);
        $('#modalKepengurusan select[name="sekretaris"]').val(data.sekretaris);

        $('#modalKepengurusan input[name="tgl_mulai"]').val(data.tgl_mulai);
        $('#modalKepengurusan input[name="tgl_selesai"]').val(data.tgl_selesai);

        $('#modalKepengurusan button[type="submit"]').text("Save");
      },
    });
  });
});
