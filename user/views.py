from django.shortcuts import render, redirect
from django.contrib import auth
from .forms import LoginForm

class AuthView:
    def login(request):
        if request.method == "POST":
            form = LoginForm(request.POST)
            if form.is_valid():
                username = form.cleaned_data['username']
                password = form.cleaned_data['password']
                user = auth.authenticate(request, username=username, password=password)
                if user is not None:
                    auth.login(request, user)
                    return redirect('dashboard')
                else:
                    return render(request, 'auth/login.html', {'form': form, 'error_message': 'Invalid username or password.'})
 

        else:
            if request.user.is_authenticated:
                return redirect('dashboard')

            form = LoginForm()
            context = {
                'form': form,
            }
            return render(request, 'auth/login.html', context)

    
    def logout(request):
        auth.logout(request)

        return redirect('login')
