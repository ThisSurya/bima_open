from django.core.exceptions import PermissionDenied
from allauth.socialaccount.signals import pre_social_login
from allauth.account.signals import user_logged_in
from django.dispatch import receiver
from mahasiswa.models import Mahasiswa
from django.db.models import Q
from ormawa.models import Kepengurusan
from django.contrib.auth.models import User
from django.shortcuts import redirect
from datetime import date


@receiver(pre_social_login)
def social_login_user_check(request, sociallogin, **kwargs):
    address, domain = sociallogin.user.email.split('@')

    # validate email domain=
    if domain == "mhs.dinus.ac.id":
        # convert nim by addres of email
        nim = str(chr(64 + int(address[0]))) + address[1:3] + '.' + address[3:7] + '.' + address[7:]

        # check is nim on mahasiswa list
        mhsFound = Mahasiswa.objects.filter(nim=nim).first()
        if not mhsFound:
            print("error nim not found")
            raise PermissionDenied("Email tidak valid.")

        kepengurusanFound = Kepengurusan.objects.filter(Q(ketua=mhsFound) | Q(sekretaris=mhsFound) & Q(periode__tgl_selesai__gt=date.today().strftime("%Y-%m-%d")))
        if not kepengurusanFound:
            raise PermissionDenied("Kepengurusan sudah tidak berlaku.")
        
        userFound = User.objects.filter(username=nim).first()
        if userFound:
            sociallogin.user = userFound
            return

        # change username to nim
        sociallogin.user.username = nim

@receiver(user_logged_in)
def redirect_user_after_login(request, user, **kwargs):
    print('user loggged in')
    return redirect('dashboard')