from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.contrib.auth.decorators import permission_required
from .models import Mahasiswa, Prestasi
from .forms import PrestasiForm
import json

# Create your views here.
class MahasiswaView:

    @permission_required('mahasiswa.view_mahasiswa')
    def index(request):
        mahasiswa = Mahasiswa.objects.all()

        context = {
            "mahasiswa": mahasiswa,
        }
        return render(request, 'admin/mahasiswa/index.html', context)

    def search(request):
        if request.method == "POST":
            if request.content_type == 'text/plain':
                body = json.loads(request.body)
                mahasiswa = Mahasiswa.objects.filter(nim__icontains=body['query']).all()
                
                data = []
                for m in mahasiswa:
                    data.append({
                        'id': m.id,
                        'nama': f"{m.nim} - {m.nama}",
                    })
                return JsonResponse(data, safe=False)
            else:
                print(request.content_type)
                query_search = request.POST.get('query')

                mahasiswa = Mahasiswa.objects.filter(nim=query_search).first()
                print(mahasiswa)

                context = {
                    "found": True if mahasiswa != 0 else False,
                    "mahasiswa": mahasiswa,
                }
                return render(request, 'admin/mahasiswa/search.html', context)

        else:
            print("lah")

            return render(request, 'admin/mahasiswa/search.html')
        
class PrestasiView:
    
    @permission_required('mahasiswa.view_prestasi')
    def index(request):
        context = {
            "prestasi": Prestasi.objects.all(),
            "form": PrestasiForm(),
        }
        return render(request, 'admin/prestasi/index.html', context)
    
    @permission_required('mahasiswa.add_prestasi')
    def create(request):
        if request.method == "POST":
            form = PrestasiForm(request.POST)
            if form.is_valid():
                Prestasi.objects.create(
                    nama=form.cleaned_data['nama'],
                    tingkat=form.cleaned_data['tingkat'],
                    mahasiswa=Mahasiswa.objects.get(nim=form.cleaned_data['mahasiswa'])
                )
                return redirect('prestasi.index')
    
    @permission_required('mahasiswa.change_prestasi')
    def edit(request, prestasiID):
        if request.method == "POST":
            form = PrestasiForm(request.POST)
            if form.is_valid():
                print(form.cleaned_data['nama'])
                prestasi = Prestasi.objects.filter(id=prestasiID).first()
                prestasi.nama = form.cleaned_data['nama']
                prestasi.tingkat = form.cleaned_data['tingkat']
                prestasi.mahasiswa = Mahasiswa.objects.get(nim=form.cleaned_data['mahasiswa'])
                prestasi.save()

                return redirect('prestasi.index')

    @permission_required('mahasiswa.delete_prestasi')
    def delete(request, prestasiID):
        if request.method == "POST":
            Prestasi.objects.filter(id=prestasiID).delete()
            return redirect('prestasi.index')
