from django.urls import path
from django.contrib.auth.decorators import login_required
from . import views


urlpatterns = [
    path("mahasiswa", login_required(views.MahasiswaView.index), name="mahasiswa.index"),
    path("mahasiswa/search", login_required(views.MahasiswaView.search), name="mahasiswa.search"),

    path("prestasi", login_required(views.PrestasiView.index), name="prestasi.index"),
    path("prestasi/create", login_required(views.PrestasiView.create), name="prestasi.create"),
    path("prestasi/edit/<int:prestasiID>", login_required(views.PrestasiView.edit), name="prestasi.edit"),
    path("prestasi/delete/<int:prestasiID>", login_required(views.PrestasiView.delete), name="prestasi.delete"),
]