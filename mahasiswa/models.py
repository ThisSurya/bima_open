from django.db import models
from django.contrib.auth.models import User


class Mahasiswa(models.Model):
    id = models.AutoField(primary_key=True)
    nim = models.CharField(max_length=255)
    nama = models.CharField(max_length=255)
    user = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True)
    status_mhs = models.IntegerField()
    ipk = models.FloatField()

    def __str__(self):
        return f'{self.nim} - {self.nama}'

class Prestasi(models.Model):
    id = models.AutoField(primary_key=True)
    nama = models.CharField(max_length=255)
    tingkat = models.CharField(max_length=255)
    mahasiswa = models.ForeignKey(Mahasiswa, on_delete=models.CASCADE)
