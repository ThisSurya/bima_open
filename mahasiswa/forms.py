from django import forms
from .models import Prestasi

class PrestasiForm(forms.Form):
    nama = forms.CharField(
        label="Nama Prestasi",
        widget=forms.TextInput(
            attrs={
                'x-model': 'prestasiEdit.nama',
                'placeholder':'Masukan Nama Prestasi',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            },
        ),
    )
    tingkat = forms.CharField(
        label="Tingkat Prestasi",
        widget=forms.TextInput(
            attrs={
                'x-model': 'prestasiEdit.tingkat',
                'placeholder':'Masukan Tingkat Prestasi',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            },
        ),
    )
    mahasiswa = forms.CharField(
        label="NIM Mahasiswa",
        widget=forms.TextInput(
            attrs={
                'x-model': 'prestasiEdit.mahasiswa',
                'placeholder':'Masukan NIM Mahasiswa',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            },
        ),
    )