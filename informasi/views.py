from django.shortcuts import render, redirect
from .forms import BeritaForm, PengumumanForm
from .models import Berita, Pengumuman

# Create your views here.
class BeritaView:
    def index(request):
        context = {
            'form': BeritaForm(),
            'berita': Berita.objects.all(),
        }
        return render(request, 'admin/berita/index.html', context)

    def create(request):
        if request.method == "POST":
            form = BeritaForm(request.POST)
            if form.is_valid():
                form.save()
                return redirect("berita.index")

    def edit(request, beritaID):
        if request.method == "POST":
            berita = Berita.objects.get(id=beritaID)
            form = BeritaForm(request.POST, instance=berita)
            if form.is_valid():
                form.save()
                return redirect('berita.index')

    def delete(request, beritaID):
        if request.method == "POST":
            Berita.objects.get(id=beritaID).delete()
            return redirect('berita.index')


class PengumumanView:

    def index(request):
        context = {
            "pengumuman": Pengumuman.objects.all(),
            "form": PengumumanForm(),
        }
        return render(request, 'admin/pengumuman/index.html', context)

    def create(request):
        if request.method == "POST":
            form = PengumumanForm(request.POST)
            if form.is_valid():
                p = form.save(commit=False)
                p.broadcast_mhs = 1 if form.cleaned_data.get('broadcast_mhs') else 0
                p.broadcast_dsn = 1 if form.cleaned_data.get('broadcast_dsn') else 0
                p.save()
                print(form.cleaned_data.get('broadcast_mhs'))
                print(form.cleaned_data.get('broadcast_dsn'))
                return redirect("pengumuman.index")


    def edit(request, pengumumanID):
        if request.method == "POST":
            pengumuman = Pengumuman.objects.get(id=pengumumanID)
            form = PengumumanForm(request.POST, instance=pengumuman)
            if form.is_valid():
                form.save()
                return redirect("pengumuman.index")

    def delete(request, pengumumanID):
        if request.method == "POST":
            Pengumuman.objects.get(id=pengumumanID).delete()
            return redirect("pengumuman.index")
    

class SambutanView:

    def index(request):
        None

    def create(request):
        if request.method == "POST":
            form = BeritaForm(request.POST)
            if form.is_valid():
                form.save()
            return redirect("berita.create")
        else:
            form = BeritaForm()
            return render(request, "create.html", {"form": form})
    
    def show(request):
        None

    def edit(request):
        None

    def delete(request):
        None