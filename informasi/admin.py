from django.contrib import admin

from .models import Berita, Sambutan, Pengumuman
# Register your models here.
@admin.register(Berita)
class BeritaAdmin(admin.ModelAdmin):
    list_display = ('judul', 'createdAt')

@admin.register(Sambutan)
class SambutanAdmin(admin.ModelAdmin):
    # list_display = ('judul', 'createdAt')
    None

@admin.register(Pengumuman)
class PengumumanAdmin(admin.ModelAdmin):
    # list_display = ('judul', 'createdAt')
    None