from django.db import models
from datetime import date

# Create your models here.
class Berita(models.Model):
    id = models.BigAutoField(primary_key=True)
    judul = models.CharField(max_length=100)
    content = models.TextField()
    sender = models.CharField(max_length=100)
    createdAt = models.DateField()
    expiredAt = models.DateField(null=True)
    
    class Meta:
        db_table = 'berita'


class Pengumuman(models.Model):
    id = models.BigAutoField(primary_key=True)
    judul = models.CharField(max_length=100)
    content = models.TextField()
    sender = models.CharField(max_length=100)
    broadcast_mhs = models.BooleanField(default=False)
    broadcast_dsn = models.BooleanField(default=False)
    createdAt = models.DateField()


class Sambutan(models.Model):
    id = models.BigAutoField(primary_key=True)
    createdAt = models.DateField()

